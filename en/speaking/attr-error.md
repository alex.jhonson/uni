# introspection error
- bridge study by Dutton and Aron (1974)
    - [](/un/en/speaking/bridge.png)
    - [](/un/en/speaking/bridge-result.png)
- reason-generated attitude change by Wilson and Kraft (1993)
    - students tried to explain 'hard to explain' attitude toward current relationships with pros/cons lists
        - results
            - pros+ -> positive attitudes change
            - cons+ -> negative attitudes change
        - change returned to 'hard to explain' in a while, hence bad break up decision might be taken
        - introspection isn't always wrong, but happens
