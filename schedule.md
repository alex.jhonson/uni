- mon
    1. eng
        - time: 8:30 - 9:50
        - room: 437
        - teacher: Черниш
    2. probabilistic theory [seminar]
        - time: 10:10 -> 11:30
        - room: 437
        - teacher: Жолткевич Нина Андреевна
    3. intro [lecture] `blinking` **on**
        - time: 12:00 -> 13:20
        - room: 435
- tue
    2. experimental psy and practice [lecture] `blinking` **off**
        - time: 10:10 -> 11:30
        - room: 435
        - teacher: Кочарян Игорь Александрович
    3. experimental psy and practice [seminar]
        - time: 12:00 -> 13:20
        - room: 437
        - teacher: Кочарян Игорь Александрович
    4. general psy [seminar] 
        - time: 13:40 -> 15:00
        - room: 437
        - teacher: Москаленко Виктория Васильвна
- wed
    1. physiology [lecture]
        - time: 8:30 -> 9:50
        - room: 435
        - teacher: Забродський Руслан Франсович
    2. probabilistic theory [lecture]
        - time: 10:10 -> 11:30
        - room: 435
        - teacher: Назиров З.Ф.
    4. curator's hour
        - time: 13:40 -> 15:00
        - room: 435
        - teacher: Гуляєва Елена Владимировна
- thu
    2. physiology [seminar] `blinking` **off**
        - time: 10:10 -> 11:30
        - room: 2-22
        - teacher: Забродський Руслан Франсович
    3. eng
        - time: 12:00 -> 13:20
        - room: 415
        - teacher: Черниш
- fri
    1. history [lecture] 
        - time: 8:30 -> 9:50
        - room: 450
        - teacher: Домановский Андрей Николаевич
    2. history [seminar] `blinking` **off**
        - time: 10:10 -> 11:30
        - room: 447
        - teacher: Домановский Андрей Николаевич
    3. general psy [lecture]
        - time: 12:00 -> 13:20
        - room: 435
        - teacher: Москаленко Виктория Васильвна
